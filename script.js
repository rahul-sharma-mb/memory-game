//      level selector
var level = "easyLevel";
const easyFunc = () => {
  level = "easyLevel";
  const gameContainer = document.getElementById("game");
  gameContainer.innerHTML = "";
  document.getElementById("p1").innerHTML = 0;
  easy();
};
const mediumFunc = () => {
  level = "mediumLevel";
  const gameContainer = document.getElementById("game");
  gameContainer.innerHTML = "";
  document.getElementById("p1").innerHTML = 0;
  medium();
};
const hardFunc = () => {
  level = "hardLevel";
  const gameContainer = document.getElementById("game");
  gameContainer.innerHTML = "";
  document.getElementById("p1").innerHTML = 0;
  hard();
};

//game reset
const reset = () => {
  if (level == "easyLevel") {
    easyFunc();
  } else if (level == "mediumLevel") {
    mediumFunc();
  } else {
    hardFunc();
  }
};

//                             game modes

function easy() {
  const gameContainer = document.getElementById("game");
  gameContainer.innerHTML = "";

  const COLORS = [
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "red",
    "blue",
    "green",
    "orange",
    "purple",
  ];

  // here is a helper function to shuffle an array
  // it returns the same array with values shuffled
  // it is based on an algorithm called Fisher Yates if you want ot research more
  function shuffle(array) {
    let counter = array.length,
      temp,
      index; //10

    // While there are elements in the array
    while (counter > 0) {
      // Pick a random index
      index = Math.floor(Math.random() * counter);
      console.log("index", index);

      // Decrease counter by 1
      counter--;

      // And swap the last element with it
      temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
      console.log(array[index], array[counter]);
    }
    console.log(array);

    return array;
  }

  let shuffledColors = shuffle(COLORS);

  //console.log("shuffledColors", shuffledColors);

  // this function loops over the array of colors
  // it creates a new div and gives it a class with the value of the color
  // it also adds an event listener for a click for each card
  function createDivsForColors(colorArray) {
    for (let color of colorArray) {
      // create a new div
      const newDiv = document.createElement("div");

      // give it a class attribute for the value we are looping over
      newDiv.classList.add(color);
      // newDiv.style.background = color;

      // call a function handleCardClick when a div is clicked on
      newDiv.addEventListener("click", handleCardClick);

      // append the div to the element with an id of game
      gameContainer.append(newDiv);
    }
  }

  let clickCounter = 0,
    matchedArray = [],
    clickedArray = [];
  // TODO: Implement this function!
  function handleCardClick(event) {
    clickCounter++;
    document.getElementById("p1").innerHTML = clickCounter;

    // console.log(event.target.className);
    //console.log(clickedArray, clickCounter);
    if (clickedArray.length == 0) {
      event.target.style.background = event.target.className;
      setTimeout(() => {
        event.target.style.background = "none";
      }, 500);
      clickedArray.push(event.target.className);
      // console.log(clickedArray);
    } else if (!clickedArray.includes(event.target.className)) {
      event.target.style.background = event.target.className;
      setTimeout(() => {
        event.target.style.background = "none";
      }, 500);
      // console.log("before step 1", clickedArray);
      clickedArray.splice(0, 1);
      clickedArray.push(event.target.className);
      // console.log("after step 1", clickedArray);
    } else if (clickedArray.includes(event.target.className)) {
      clickedArray.splice(0, 1);

      let val = document.getElementsByClassName(event.target.className);

      for (let i = 0; i < val.length; i++) {
        val[i].style.background = event.target.className;
      }
      if (!matchedArray.includes(event.target.className)) {
        matchedArray.push(event.target.className);
        matchedArray.push(event.target.className);
      }
      if (matchedArray.length == 10) {
        alert("well done,it took you " + clickCounter + " attempts");
      }
    }

    //console.log("you clicked", event.target.className);
  }

  // when the DOM loads
  createDivsForColors(shuffledColors);
}
// easy();

const medium = () => {
  const gameContainer = document.getElementById("game");
  gameContainer.innerHTML = "";

  const COLORS = [
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "DeepPink",
    "Coral",
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "DeepPink",
    "Coral",
  ];

  // here is a helper function to shuffle an array
  // it returns the same array with values shuffled
  // it is based on an algorithm called Fisher Yates if you want ot research more
  function shuffle(array) {
    let counter = array.length,
      temp,
      index; //10

    // While there are elements in the array
    while (counter > 0) {
      // Pick a random index
      index = Math.floor(Math.random() * counter);
      console.log("index", index, counter);

      // Decrease counter by 1
      counter--;

      // And swap the last element with it
      temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
      console.log(temp, array[counter]);
    }
    console.log(array);

    return array;
  }

  let shuffledColors = shuffle(COLORS);

  //console.log("shuffledColors", shuffledColors);

  // this function loops over the array of colors
  // it creates a new div and gives it a class with the value of the color
  // it also adds an event listener for a click for each card
  function createDivsForColors(colorArray) {
    for (let color of colorArray) {
      // create a new div
      const newDiv = document.createElement("div");

      // give it a class attribute for the value we are looping over
      newDiv.classList.add(color);
      // newDiv.style.background = color;

      // call a function handleCardClick when a div is clicked on
      newDiv.addEventListener("click", handleCardClick);

      // append the div to the element with an id of game
      gameContainer.append(newDiv);
    }
  }

  let clickCounter = 0,
    matchedArray = [],
    clickedArray = [];
  // TODO: Implement this function!
  function handleCardClick(event) {
    clickCounter++;
    document.getElementById("p1").innerHTML = clickCounter;

    // console.log(event.target.className);
    //console.log(clickedArray, clickCounter);
    if (clickedArray.length == 0) {
      event.target.style.background = event.target.className;
      setTimeout(() => {
        event.target.style.background = "none";
      }, 500);
      clickedArray.push(event.target.className);
      // console.log(clickedArray);
    } else if (!clickedArray.includes(event.target.className)) {
      event.target.style.background = event.target.className;
      setTimeout(() => {
        event.target.style.background = "none";
      }, 500);
      // console.log("before step 1", clickedArray);
      clickedArray.splice(0, 1);
      clickedArray.push(event.target.className);
      // console.log("after step 1", clickedArray);
    } else if (clickedArray.includes(event.target.className)) {
      clickedArray.splice(0, 1);

      let val = document.getElementsByClassName(event.target.className);

      for (let i = 0; i < val.length; i++) {
        val[i].style.background = event.target.className;
      }
      if (!matchedArray.includes(event.target.className)) {
        matchedArray.push(event.target.className);
        matchedArray.push(event.target.className);
      }
      if (matchedArray.length == 14) {
        alert("well done,it took you " + clickCounter + " attempts");
      }
      //console.log("jajk", clickCounter);
    }

    //console.log("you clicked", event.target.className);
  }

  // when the DOM loads
  createDivsForColors(shuffledColors);
};
//medium();
//easy();

const hard = () => {
  const gameContainer = document.getElementById("game");
  gameContainer.innerHTML = "";

  const COLORS = [
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "DeepPink",
    "Coral",
    "brown",
    "black",
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "DeepPink",
    "Coral",
    "brown",
    "black",
  ];

  // here is a helper function to shuffle an array
  // it returns the same array with values shuffled
  // it is based on an algorithm called Fisher Yates if you want ot research more
  function shuffle(array) {
    let counter = array.length,
      temp,
      index; //10

    // While there are elements in the array
    while (counter > 0) {
      // Pick a random index
      index = Math.floor(Math.random() * counter);
      // console.log("index", index, counter);

      // Decrease counter by 1
      counter--;

      // And swap the last element with it
      temp = array[counter];
      array[counter] = array[index];
      array[index] = temp;
      //console.log(temp, array[counter]);
    }
    // console.log(array);

    return array;
  }

  let shuffledColors = shuffle(COLORS);

  //console.log("shuffledColors", shuffledColors);

  // this function loops over the array of colors
  // it creates a new div and gives it a class with the value of the color
  // it also adds an event listener for a click for each card
  function createDivsForColors(colorArray) {
    for (let color of colorArray) {
      // create a new div
      const newDiv = document.createElement("div");

      // give it a class attribute for the value we are looping over
      newDiv.classList.add(color);
      // newDiv.style.background = color;

      // call a function handleCardClick when a div is clicked on
      newDiv.addEventListener("click", handleCardClick);

      // append the div to the element with an id of game
      gameContainer.append(newDiv);
    }
  }

  let clickCounter = 0,
    matchedArray = [],
    clickedArray = [];
  // TODO: Implement this function!
  function handleCardClick(event) {
    clickCounter++;
    document.getElementById("p1").innerHTML = clickCounter;

    // console.log(event.target.className);
    //console.log(clickedArray, clickCounter);
    if (clickedArray.length == 0) {
      event.target.style.background = event.target.className;
      setTimeout(() => {
        event.target.style.background = "none";
      }, 500);
      clickedArray.push(event.target.className);
      console.log(clickedArray);
    } else if (!clickedArray.includes(event.target.className)) {
      event.target.style.background = event.target.className;
      setTimeout(() => {
        event.target.style.background = "none";
      }, 500);
      //console.log("before step 1", clickedArray);
      clickedArray.splice(0, 1);
      clickedArray.push(event.target.className);
      console.log("after step 1", clickedArray);
    } else if (clickedArray.includes(event.target.className)) {
      clickedArray.splice(0, 1);

      let val = document.getElementsByClassName(event.target.className);

      for (let i = 0; i < val.length; i++) {
        val[i].style.background = event.target.className;
      }
      if (!matchedArray.includes(event.target.className)) {
        matchedArray.push(event.target.className);
        matchedArray.push(event.target.className);
      }

      if (matchedArray.length == 18) {
        alert("well done,it took you " + clickCounter + " attempts");
      }
      //console.log("jajk", clickCounter);
    }

    //console.log("you clicked", event.target.className);
  }

  // when the DOM loads
  createDivsForColors(shuffledColors);
};
// hard();

easy();
